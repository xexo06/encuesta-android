package com.example.encuesta3;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText et_rut, et_clave;
    Button bt_entrar;
    TextView tv_crearCuenta, tv_graficos,tv_politicas;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        et_rut = (EditText) findViewById(R.id.et_rut);
        et_clave = (EditText)findViewById(R.id.et_clave);
        bt_entrar = (Button) findViewById(R.id.bt_ingresar);
        tv_crearCuenta =  (TextView) findViewById(R.id.tv_crearCuenta);
        tv_graficos = (TextView) findViewById(R.id.tv_graficos);
        tv_politicas = (TextView) findViewById(R.id.tv_politicas);


        tv_graficos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LoginGrafico.class);
                startActivity(intent);
            }
        });

        tv_crearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),CrearCuenta.class);
                startActivity(intent);
            }
        });

        tv_politicas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),Politicas.class);
                startActivity(intent);
            }
        });

        bt_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String registro="http://ip/Android_Encuesta/Login/login.php?rut="+et_rut.getText().toString();
                ingresarLogin(registro);
                new MainActivity.AsyncTask_load().execute();
                bt_entrar.setClickable(false);
            }
        });
    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(3);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_entrar.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

    public void ingresarLogin(String URL) {



        //  Toast.makeText(getApplicationContext(), "funciona" + URL, Toast.LENGTH_LONG).show();


        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {




                ObjectEmpresa empresa = new ObjectEmpresa();
                JSONArray jsonArray = response.optJSONArray("datos");
                JSONObject jsonObject = null;


                try {
                    Toast.makeText(getApplicationContext(), "Cargando", Toast.LENGTH_SHORT).show();
                    jsonObject = jsonArray.getJSONObject(0);
                    empresa.setId(jsonObject.optString("id"));
                    empresa.setNombre(jsonObject.optString("nombre"));
                    empresa.setRut(jsonObject.optString("rut"));
                    empresa.setDireccion(jsonObject.optString("direccion"));
                    empresa.setTelefono(jsonObject.optString("telefono"));
                    empresa.setClave(jsonObject.optString("clave"));

                    if (empresa.getClave().equalsIgnoreCase(et_clave.getText().toString()) && !et_rut.getText().toString().isEmpty()) {



                        Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                        Bundle b = new Bundle();
                        String [] arreglo = new String[6];
                        arreglo[0] = empresa.getId();
                        arreglo[1] = empresa.getNombre();
                        arreglo[2] = empresa.getRut();
                        arreglo[3] = empresa.getDireccion();
                        arreglo[4] = empresa.getTelefono();
                        arreglo[5] = empresa.getClave();

                        b.putStringArray("empresa",arreglo);
                        intent.putExtras(b);

                        startActivity(intent);

                    }else{

                        Toast.makeText(getApplicationContext(), "no coincide la clave", Toast.LENGTH_SHORT).show();

                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "hubo un error", Toast.LENGTH_SHORT).show();
            }
        }

        );

        queue.add(jsonObjectRequest);


    }

}
