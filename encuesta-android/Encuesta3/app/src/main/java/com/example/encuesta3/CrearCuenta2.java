package com.example.encuesta3;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class CrearCuenta2 extends AppCompatActivity {

    EditText et_clave, et_clave2;
    Button   bt_siguiente;
    String nombre,rut,direccion,telefono;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta2);

        et_clave = (EditText) findViewById(R.id.et_clave);
        et_clave2 = (EditText) findViewById(R.id.et_clave2);

        bt_siguiente = (Button) findViewById(R.id.bt_siguiente);

        nombre=getIntent().getStringExtra("nombre");
        rut = getIntent().getStringExtra("rut");
        direccion = getIntent().getStringExtra("direccion");
        telefono = getIntent().getStringExtra("telefono");

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        bt_siguiente.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if(et_clave.getText().toString().equalsIgnoreCase(et_clave2.getText().toString()) && !et_clave.getText().toString().isEmpty()) {



                    String registro = "http://ip/Android_Encuesta/Empresa/agregarEmpresa.php?nombre=" + nombre + "&rut=" + rut +
                            "&direccion=" + direccion + "&telefono=" + telefono + "&clave=" + et_clave.getText();
                    agregarEmpresa(registro);
                    onBackPressed();
                }else{
                    Toast.makeText(getApplicationContext(),"la claves no coinciden",Toast.LENGTH_LONG).show();
                }
                new CrearCuenta2.AsyncTask_load().execute();
                bt_siguiente.setClickable(false);
            }
        });
    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(20);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_siguiente.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

    private void agregarEmpresa(String URL) {



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");




                if (response.length() > 4) {

                    Toast.makeText(getApplicationContext(), "se agregó con éxito", Toast.LENGTH_LONG).show();


                } else {
                    Toast.makeText(getApplicationContext(), "El rut ya existe", Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);

    }


}
