package com.example.encuesta3;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.channels.InterruptedByTimeoutException;

public class CrearCuenta extends AppCompatActivity {

    TextView tv_nombre, tv_rut, tv_direccion, tv_telefono;
    Button bt_siguiente;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);

        tv_nombre = (TextView) findViewById(R.id.tv_nombre);
        tv_rut = (TextView) findViewById(R.id.tv_rut);
        tv_direccion = (TextView) findViewById(R.id.tv_direccion);
        tv_telefono = (TextView) findViewById(R.id.tv_telefono);

        bt_siguiente = (Button) findViewById(R.id.bt_siguiente);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);


        bt_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), CrearCuenta2.class);
                intent.putExtra("nombre", tv_nombre.getText().toString());
                intent.putExtra("rut", tv_rut.getText().toString());
                intent.putExtra("direccion", tv_direccion.getText().toString());
                intent.putExtra("telefono", tv_telefono.getText().toString());
                if (tv_nombre.getText().toString().isEmpty() || tv_rut.getText().toString().isEmpty()
                || tv_direccion.getText().toString().isEmpty() || tv_telefono.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Faltaron datos,Llene el formulario completo", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                    startActivity(intent);
                }
                new CrearCuenta.AsyncTask_load().execute();
                bt_siguiente.setClickable(false);
            }

        });



    }
    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(20);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_siguiente.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

}
