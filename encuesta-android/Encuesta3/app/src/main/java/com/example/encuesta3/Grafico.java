package com.example.encuesta3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Grafico extends AppCompatActivity {

    Spinner spinner;

    int mes;


    private Button bt;
    private String[] arreglo;
    private BarChart barChart;



    //ejes X and Y
    private String[]meses=new String[]{"Enero", "Febrero", "Marzo", "Abril", "Mayo"};
    private int[] ventas = new int[]{25, 20, 44, 32, 15};

    //guardar colores
  //  int[] color = new int[]{Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.CYAN};
    int[] color = new int[]{Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.RED, Color.RED,Color.RED,
            Color.CYAN,Color.CYAN,Color.CYAN,Color.CYAN};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);

     final String[] empresa= getIntent().getStringArrayExtra("empresa");


        barChart = (BarChart) findViewById(R.id.barChart);
        bt = (Button) findViewById(R.id.bt_siguiente);

        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.atributos_meses,R.layout.spinner_item_primary);

        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(parent.getContext(),"selecionado" +parent.getItemAtPosition(position).toString(),Toast.LENGTH_LONG).show();

                mes=position+1;
                String mess= Integer.toString(mes);
               // Toast.makeText(getApplicationContext(), "="+empresa[0]+mes, Toast.LENGTH_SHORT).show();




               String registro = "http://ip/Android_Encuesta/Grafica/obtenerGraficoMensual.php?fkCliente="+empresa[0]+
                       "&mes="+mess.toString();
                obtenerDatos(registro);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCharts();
            }
        });


    }

    private void obtenerDatos(String URL) {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");



                if (response.length() > 0) {
                    try {
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        //Toast.makeText(getApplicationContext(), "Cargado", Toast.LENGTH_SHORT).show();

                        arreglo=new String[ja.length()];
                        for(int i=0; i<arreglo.length;i++){
                            arreglo[i]=ja.get(i).toString();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(getApplicationContext(), "Los datos fueron enviados Verifique", Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }


    private Chart getSameChart(Chart chart, String description, int textColor, int background, int animateY) {

        chart.getDescription().setText(description);
        chart.getDescription().setTextSize(15);
        chart.setBackgroundColor(background);
        chart.animateY(animateY);
        legend(chart);
        return chart;

    }

    private void legend(Chart chart){

        Legend legend = chart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

        ArrayList<LegendEntry> entries =new ArrayList<>();

        for (int i=0; i<meses.length;i++) {
            LegendEntry entry = new LegendEntry();
            entry.formColor = color[i];
            entry.label = meses[i];
            entries.add(entry);
        }
        legend.setCustom(entries);
    }

    private ArrayList<BarEntry>getBarEntries() {
        ArrayList<BarEntry> entries = new ArrayList<>();

        for (int i = 0; i < arreglo.length; i++) {
            float promedio;
            promedio = Float.parseFloat(arreglo[i]);
            //tenis ventas[i]
            entries.add(new BarEntry(i,promedio));


        }
        return entries;
    }

    private void axisX(XAxis axis){
        axis.setGranularityEnabled(true);
        axis.setPosition(XAxis.XAxisPosition.BOTTOM);
        String[] pregunta =new String[arreglo.length];
        for(int i=0 ; i<arreglo.length;i++) {
            pregunta[i]="pregunta"+(i+1);
        }
        axis.setValueFormatter(new IndexAxisValueFormatter(pregunta));
    }

    private void axisLeft(YAxis axis){
        axis.setSpaceTop(30);
        axis.setAxisMinimum(0);
    }
    private void axisRight(YAxis axis){
        axis.setEnabled(false);
    }

    public void createCharts() {
        barChart = (BarChart) getSameChart(barChart, "series", Color.RED, Color.GREEN, 3000);
        barChart.setDrawGridBackground(true);
        barChart.setDrawBarShadow(true);
        barChart.setData(getBarData());
        barChart.invalidate();
        axisX(barChart.getXAxis());
        axisLeft(barChart.getAxisLeft());
        axisRight(barChart.getAxisRight());

    }
    private DataSet getData(DataSet dataSet ){
        dataSet.setColors(color);
        dataSet.setValueTextSize(Color.WHITE);
        dataSet.setValueTextSize(10);
        return dataSet;
    }

    private BarData getBarData(){
        BarDataSet barDataSet=(BarDataSet)getData(new BarDataSet(getBarEntries(),""));

        barDataSet.setBarShadowColor(Color.GRAY);
        BarData barData= new BarData(barDataSet);
        barData.setBarWidth(0.45f);
        return barData;
    }
}
