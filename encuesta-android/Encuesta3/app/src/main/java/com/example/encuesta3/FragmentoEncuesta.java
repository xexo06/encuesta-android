package com.example.encuesta3;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

public class FragmentoEncuesta extends Fragment {

    RadioButton rb1, rb2, rb3, rb4, rb5, rb6, rb7, rb8, rb9, rb10;
    TextView pregunta;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_encuesta, container, false);

        rb1 = (RadioButton) vista.findViewById(R.id.radioButton);
        rb2 = (RadioButton) vista.findViewById(R.id.radioButton2);
        rb3 = (RadioButton) vista.findViewById(R.id.radioButton3);
        rb4 = (RadioButton) vista.findViewById(R.id.radioButton4);
        rb5 = (RadioButton) vista.findViewById(R.id.radioButton5);
        rb6 = (RadioButton) vista.findViewById(R.id.radioButton6);
        rb7 = (RadioButton) vista.findViewById(R.id.radioButton7);
        rb8 = (RadioButton) vista.findViewById(R.id.radioButton8);
        rb9 = (RadioButton) vista.findViewById(R.id.radioButton9);
        rb10 = (RadioButton) vista.findViewById(R.id.radioButton10);

        pregunta = (TextView) vista.findViewById(R.id.tv_pregunta);

        pregunta.setText("¿" + getArguments().getString("pregunta") + "?");

        return vista;

    }
}
