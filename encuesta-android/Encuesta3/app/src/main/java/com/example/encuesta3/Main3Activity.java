package com.example.encuesta3;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity {


    int i=0;
    String nivel,id_empresa,fkEncuesta;
    String[] niveles = new String[10];
    Button bt_responder;
    ArrayList<String> ListaPreguntas = new ArrayList<>();
    ArrayList<String> listaNiveles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);


        final Handler handler = new Handler();

        //boton de respuesta xd
        bt_responder = (Button) findViewById(R.id.bt_responder);


         //para crear la encuesta
        id_empresa=(String) getIntent().getSerializableExtra("id_empresa");
        //Toast.makeText(getApplicationContext(),"id_MEpresa="+id_empresa,Toast.LENGTH_LONG).show();

        // lista niveles es para una futura modificacion donde se guarde los datos al momento de terminar  la encuesta
        listaNiveles=(ArrayList<String>) getIntent().getSerializableExtra("listaNiveles");
       // Toast.makeText(getApplicationContext(),"ListaNiveles="+listaNiveles,Toast.LENGTH_LONG).show();



        // lista de todas la preguntas  en el siguiente formato [1,pregunta1,2,pregunta2....]
        ListaPreguntas = (ArrayList<String>) getIntent().getSerializableExtra("ListaPreguntas");
       // Toast.makeText(getApplicationContext(),"Lista="+ListaPreguntas,Toast.LENGTH_LONG).show();


        // le paso la pregunta al framento
        final FragmentoEncuesta encuesta= new FragmentoEncuesta();
        String pregunta=ListaPreguntas.get(1);
        Bundle argumento = new Bundle();
        argumento.putString("pregunta",pregunta);
        encuesta.setArguments(argumento);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,
                encuesta).commit();

        bt_responder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (encuesta.rb1.isChecked()) {
                    nivel = "1";
                    listaNiveles.add("1");

                } else if (encuesta.rb2.isChecked()) {
                    nivel = "2";
                    listaNiveles.add("2");

                } else if (encuesta.rb3.isChecked()) {
                    nivel = "3";
                    listaNiveles.add("3");

                } else if (encuesta.rb4.isChecked()) {
                    nivel = "4";
                    listaNiveles.add("4");

                } else if (encuesta.rb5.isChecked()) {
                    nivel = "5";
                    listaNiveles.add("5");

                } else if (encuesta.rb6.isChecked()) {
                    nivel = "6";
                    listaNiveles.add("6");
                } else if (encuesta.rb7.isChecked()) {
                    nivel = "7";
                    listaNiveles.add("7");
                } else if (encuesta.rb8.isChecked()) {
                    nivel = "8";
                    listaNiveles.add("8");
                } else if (encuesta.rb9.isChecked()) {
                    nivel = "9";
                    listaNiveles.add("9");
                } else if (encuesta.rb10.isChecked()) {
                    nivel = "10";
                    listaNiveles.add("10");
                }


                if(nivel !=null) {


                    ListaPreguntas.remove(0);
                    ListaPreguntas.remove(0);
                    ListaPreguntas.remove(0);

                    if(ListaPreguntas.size()==0){
                       // acaba llega al final de la encuesta donde debemos ingresar todos los datos a la base
                        // de datos


                        // le tengo que pasar

                          // id de empresa => para crear la encuesta
                        String registro2 = "http://ip/Android_Encuesta/Encuesta/crearEncuesta.php?fk_encuesta="+id_empresa;
                        CrearEncuesta(registro2);


                        handler.postDelayed(new Runnable() {
                            public void run() {
                                Toast.makeText(getApplicationContext(),"id_Encuesta="+fkEncuesta,Toast.LENGTH_LONG).show();
                                String registro="http://ip/Android_Encuesta/Pregunta_Encuesta/guardarPreguntasEncuesta.php?fk_encuesta="+fkEncuesta+"&&lista_niveles="+listaNiveles;
                                guardarRespuesta(registro);
                            }
                        }, 2000); // 2 segundos de "delay"


                        onBackPressed();
                        // segundo obtener el id de la encuesta para guardar las preguntas

                           // guardar las preguntas



                    }else {
                        //tengo que pasar los datos de forma recursiva
                        Intent intent = getIntent();
                        intent.putExtra("ListaPreguntas", ListaPreguntas);
                        intent.putExtra( "listaNiveles",listaNiveles);

                        startActivity(intent);
                        finish();


                    }

                }


            }
        });

    }

    public void guardarRespuesta(String URL) {

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {




                ObjectEmpresa empresa = new ObjectEmpresa();
                JSONArray jsonArray = response.optJSONArray("datos");
                JSONObject jsonObject = null;




            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "asdasdas2", Toast.LENGTH_SHORT).show();
            }
        }

        );

        queue.add(jsonObjectRequest);


    }

    public void CrearEncuesta(String URL) {


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");




                if (response.length() > 0) {
                    Toast.makeText(getApplicationContext(), "Cargando", Toast.LENGTH_SHORT).show();
                    try {
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        fkEncuesta = ja.get(0).toString();
                       // Toast.makeText(getApplicationContext(),"id_Encuesta="+fkEncuesta,Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);



    }


}
