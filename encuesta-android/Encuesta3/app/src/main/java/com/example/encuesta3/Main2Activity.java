package com.example.encuesta3;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    Button bt_realizar;
    String arreglo,id_empresa;
    ProgressBar progressBarHorizontal, progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        final Handler handler = new Handler();

        bt_realizar = (Button) findViewById(R.id.bt_realizarEncuesta);
        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);


        String [] empresa = getIntent().getExtras().getStringArray("empresa");
        //nuevo codigo
        id_empresa=empresa[0];






        bt_realizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //debo cambiarla al activiti main 3
               /* String registro2 = "http://ip/Android_Encuesta/Encuesta/crearEncuesta.php?fk_encuesta="+id_empresa;
                CrearEncuesta(registro2);*/

                handler.postDelayed(new Runnable() {
                    public void run() {
                       String registro = "http://ip/Android_Encuesta/Preguntas/leerPreguntas.php";
                        ObtenerPreguntas(registro);
                    }
                }, 2000); // 2 segundos de "delay"

                new AsyncTask_load().execute();
                bt_realizar.setClickable(false);

            }
        });

    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(20);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_realizar.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }



    public void ObtenerPreguntas(String URL) {


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");


                if (response.length() > 0) {
                    try {
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());



                        //Bundle b = new Bundle();
                        //b.putStringArray("empresa",getIntent().getExtras().getStringArray("empresa"));
                        Intent intent = new Intent(getApplicationContext(), Main3Activity.class);
                        //intent.putExtras(b);




                        //con esto creamos la encuesta
                        intent.putExtra("id_empresa",id_empresa);


                        //Creamos un array list donde le debemos pasar un valor por defecto.
                        ArrayList<String> listaNiveles= new ArrayList<>();
                        listaNiveles.add("niveles");
                        intent.putExtra("listaNiveles",listaNiveles);

                        //pasamos todas las preguntas de la bd en el siguiente formato [1,pregunta1,2,pregunta2]
                        ArrayList<String> ListaPreguntas = TranformarJSONArray(ja);
                        intent.putExtra("ListaPreguntas", ListaPreguntas);


                        //este tengo que eliminarlo
                        intent.putExtra("id_encuesta",arreglo);





                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }

    public ArrayList<String> TranformarJSONArray(JSONArray ja) {

       /* int tamaño = ja.length();
        final String arreglo[][] = new String[tamaño][3];
        int j = 0;*/
        ArrayList<String> lista = new ArrayList<>();

        for (int i = 0; i < ja.length(); i++) {

            try {

                lista.add(ja.getString(i));
                //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

               /* arreglo[j][0] = ja.getString(i);
                arreglo[j][1] = ja.getString(i + 1);*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }
}
